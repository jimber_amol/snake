#include "snake.h"

class InputThread
{
  public:
	void run();
	void operator()(Snake *snake);

	int getch();
};