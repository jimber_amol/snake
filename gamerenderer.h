#pragma once

#include "board.h"

class GameRenderer
{
public:
  GameRenderer(Board *board);
  void render();
private:
  void generateBoardGraphics();
  void drawBoardGraphics();

  Board *board;
  char *boardFrame;
};