#include <iostream>
#include <algorithm>

#include "snake.h"

Snake::Snake(int startX, int startY) : direction('l')
{
	snakeParts.push_back(SnakePart(startX, startY));
}

void Snake::grow()
{
	snakeParts.push_back(SnakePart(snakeParts.back().x, snakeParts.back().y));
}

void Snake::setDirection(char d)
{
	direction = d;
}

int Snake::getSize()
{
	return snakeParts.size();
}

void Snake::move()
{
	snakeParts.back().x = snakeParts.front().x;
	snakeParts.back().y = snakeParts.front().y;
	SnakePart temp(snakeParts.back());
	snakeParts.pop_back();
	snakeParts.insert(snakeParts.begin(),temp);

	if (direction == 'l')
	{
		snakeParts.front().x = snakeParts.front().x - 1;
	}
	if (direction == 'r')
	{
		snakeParts.front().x = snakeParts.front().x + 1;
	}
	else if (direction == 'u')
	{
		snakeParts.front().y = snakeParts.front().y - 1;
	}
	else if (direction == 'd')
	{
		snakeParts.front().y = snakeParts.front().y + 1;
	}
}