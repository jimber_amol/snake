#include <iostream>
#include <chrono>
#include <thread>

#include "game.h"
#include "board.h"
#include "gamerenderer.h"
#include "snake.h"

Game::Game(int width, int height) : board(new Board(width, height)) // playsize is two chars smaller
{
	gameRenderer = new GameRenderer(board);
	srand(time(NULL));
}

void Game::run()
{
	generateFood();
	std::thread t1((InputThread()), board->snake);
	while (true)
	{
		update();
		gameRenderer->render();
		std::this_thread::sleep_for(std::chrono::milliseconds(150));
	}
}

void Game::update()
{
	board->snake->move();
	if (!isInBoard(board->snake->snakeParts.front().x, board->snake->snakeParts.front().y))
	{
		die();
	}

	for (std::vector<SnakePart>::iterator it = std::next(board->snake->snakeParts.begin()); it != board->snake->snakeParts.end(); ++it)
	{
		if (board->snake->getSize() > 1 && (it->x == board->snake->snakeParts.front().x && it->y == board->snake->snakeParts.front().y))
		{
			die();
		}
	}

	tryToEat();
}

void Game::generateFood()
{
	int newX;
	int newY;
	do
	{
		newX = 0 + (rand() % static_cast<int>(board->getWidth() - 0 + 1));
		newY = 0 + (rand() % static_cast<int>(board->getHeight() - 0 + 1));
	} while (!isInBoard(newX, newY));

	board->food->x = newX;
	board->food->y = newY;
}

void Game::processInput() {}

void Game::die()
{
	delete board;
	std::cout << "YOU DIED\n";
	exit(0);
}

bool Game::isInBoard(int x, int y)
{
	if ((x % board->getWidth()) == 0)
	{
		return false;
	}
	if (x == board->getWidth() - 1)
	{
		return false;
	}
	if (y == board->getHeight() - 1)
	{
		return false;
	}
	if ((y % board->getHeight()) == 0)
	{
		return false;
	}
	return true;
}

void Game::tryToEat()
{
	if (board->snake->snakeParts.front().x == board->food->x && board->snake->snakeParts.front().y == board->food->y)
	{
		board->snake->grow();
		generateFood();
	}
}