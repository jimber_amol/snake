#include "board.h"

Board::Board(int width, int height) : width(width),
									  height(height),
									  snake(new Snake(width / 2, height / 2)),
									  food(new Food())
{
}

int Board::getWidth()
{
	return width;
}

int Board::getHeight()
{
	return height;
}

int Board::getSize()
{
	return width * height;
}

Board::~Board()
{
	delete snake;
}