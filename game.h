#include "board.h"
#include "inputThread.h"
#include "gamerenderer.h"

class Game
{
  public:
	Game(int width, int height);
	void run();
	void processInput();

  private:
	void render();
	void update();
	void generateBoardGraphics();
	void die();
	void generateFood();
	bool isInBoard(int x, int y);
	void tryToEat();

	GameRenderer *gameRenderer;
	Board *board;

	char *boardFrame;
};