
#include <chrono>
#include <thread>
#include <termios.h>
#include <unistd.h>

#include "iostream"
#include "inputThread.h"


void InputThread::operator()(Snake *snake)
{
    int key = 127;
    while (1)
    {
        // std::cin >> key;
        //getchar() >> key;
        key = getch();
        if (key == 'w')
        {
            snake->setDirection('u');
        }
        else if (key == 's')
        {
            snake->setDirection('d');
        }
        else if (key == 'a')
        {
            snake->setDirection('l');
        }
        else if (key == 'd')
        {
            snake->setDirection('r');
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

}

int InputThread::getch(void)
{
    struct termios oldattr, newattr;
    int ch;
    tcgetattr( STDIN_FILENO, &oldattr );
    newattr = oldattr;
    newattr.c_lflag &= ~( ICANON | ECHO );
    tcsetattr( STDIN_FILENO, TCSANOW, &newattr );
    ch = getchar();
    tcsetattr( STDIN_FILENO, TCSANOW, &oldattr );
    return ch;
}