#pragma once

#include "snake.h"

struct Food
{
	int x;
	int y;

};

class Board
{
public:
	Board(int width, int height);
	~Board();
	Snake *snake;
	Food *food;

	int getWidth();
	int getHeight();
	int getSize();

private:
	int width;
	int height;
};
