#pragma once
#include <vector>

struct SnakePart
{
	int x;
	int y;
	SnakePart(int x, int y):x(x),y(y){}
};

class Snake
{
	public:
		Snake(int startX, int startY);
		void grow();
		int getSize();
		int getX();
		int getY();
		void move();
		void setDirection(char d);

		std::vector<SnakePart> snakeParts;
	private:
		char direction; // l left r right u up d down
};