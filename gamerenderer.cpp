#include <iostream>
#include <cstring>

#include "gamerenderer.h"
#include "board.h"

GameRenderer::GameRenderer(Board *board) : board(board), boardFrame(new char[board->getWidth() * board->getHeight()])
{
}

void GameRenderer::render()
{
    generateBoardGraphics();
    drawBoardGraphics();
}

void GameRenderer::generateBoardGraphics()
{

    std::cout << "Snake length: " << board->snake->getSize() << '\n';
    int width(board->getWidth());
    int height(board->getHeight());

    std::memset(boardFrame, ' ', width * height);                               // init everything with o
    std::memset(boardFrame, '-', width);                                        // set top border -
    std::memset(((char *)boardFrame) + ((width * height) - width), '-', width); // set bottom border -

    //generate left and right border
    for (int i = 1; i < height - 1; ++i)
    {
        boardFrame[i * width] = '|';
        boardFrame[i * width + width - 1] = '|';
    }

    for (auto &snakePart : board->snake->snakeParts) // access by reference to avoid copying
    {
        boardFrame[width * snakePart.y + snakePart.x] = 'x';
    }
    //boardFrame[width * board->snake->getY() + board->snake->getX()] = 'x';
    boardFrame[width * board->food->y + board->food->x] = '*';
}
void GameRenderer::drawBoardGraphics()
{
    int width(board->getWidth());
    int height(board->getHeight());
    for (int i = 0; i < board->getSize(); ++i)
    {
        std::cout << boardFrame[i];

        if (i % width == width - 1)
        {
            std::cout << '\n';
        }
    }
}